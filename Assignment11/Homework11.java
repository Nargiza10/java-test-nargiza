package Assignment11;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class Homework11 {
    public static void main(String[] args) {
        List<Integer> numberList = new ArrayList<>();
        ArrayList<Integer> numberListTwo = new ArrayList<>();
        List<Integer> numberListThree = new ArrayList<>();

        numberList.add(-2);
        numberList.add(-3);
        numberList.add(6);
        numberList.add(3);

        numberListTwo.add(-2);
        numberListTwo.add(-4);
        numberListTwo.add(5);
        numberListTwo.add(-3);

        numberListThree.addAll(numberList);
        numberListThree.addAll(numberListTwo);

        System.out.println(numberList);
        System.out.println(numberListTwo);
        System.out.println(numberListThree);

        System.out.println(Collections.min(numberListThree));;

    }
}