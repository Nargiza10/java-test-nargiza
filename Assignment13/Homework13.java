package Assignment13;

import java.util.HashMap;

public class Homework13 {
    public static void main(String[] args) {
        HashMap<Integer, String> addressNumber = new HashMap<>();
        addressNumber.put(1000, "Liam");
        addressNumber.put(1001, "Noah");
        addressNumber.put(1002, "Olivia");
        addressNumber.put(1003, "Emma");
        addressNumber.put(1004, "Benjamin");
        addressNumber.put(1005, "Evelyn");
        addressNumber.put(1006, "Lucas");
        System.out.println(addressNumber.get(1004));

        for (Integer i : addressNumber.keySet()) {
            if (i % 2 != 0){
                System.out.println(i + " " + addressNumber.get(i));
            }
        }
    }
}