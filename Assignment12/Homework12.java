package Assignment12;
import java.util.*;
import java.util.Collections;
public class Homework12 {
    public static void main(String[] args){
   Set<String> cohort1 = new HashSet<>();
   Set<String> cohort2 = new HashSet<>();
    cohort1.add("United States");
    cohort1.add("United States");
    cohort1.add("Ukraine");
    cohort1.add("Mexico");
        System.out.println(cohort1);
    cohort2.add("United States");
    cohort2.add("Canada");
    cohort2.add("United States");
    cohort2.add("Mexico");
        System.out.println(cohort2);
        Set<String> cohort3 = new HashSet<>();
        cohort3.addAll(cohort1);
        cohort3.addAll(cohort2);
        System.out.println(cohort3);

        var cohort4 = new TreeSet<String>();
        cohort4.addAll(cohort1);
        cohort4.addAll(cohort2);
     System.out.println(cohort4);

     cohort1.retainAll(cohort2);
     System.out.println(cohort1);

    }

}
